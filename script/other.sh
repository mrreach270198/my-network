#inspect valume
sudo docker inspect -f '{{ json .Mounts }}' c15760c70563 | jq

#working directory 
/opt/gopath/src/github.com/hyperledger/fabric/peer

#up docker 
sudo docker-compose -f docker-compose-cli.yaml up -d

#interact with cli tool
sudo docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" -e "CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/fabric-samples/reach-network/crypto-config/peerOrganizations/noureach.com/peers/devpeer/tls/ca.crt" -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/fabric-samples/reach-network/crypto-config/peerOrganizations/noureach.com/users/Admin@noureach.com/msp" -e "CORE_PEER_ADDRESS=peer0.noureach-network.com:7051" -it cli bash

export CORE_PEER_MSPCONFIGPATH=/opt/gopath/fabric-samples/reach-network/crypto-config/peerOrganizations/noureach.com/users/Admin@noureach.com/msp

export ORDERER_CA=/opt/gopath/fabric-samples/reach-network/crypto-config/ordererOrganizations/noureach.com/orderers/orderer.noureach.com/msp/tlscacerts/tlsca.noureach.com-cert.pem

#if TLS = false don't need to add "--tls --cafile $ORDERER_CA"
peer channel create -o orderer.noureach.com:7050 -c mychannel -f /opt/gopath/fabric-samples/reach-network/channel-artifacts/channel.tx --tls --cafile $ORDERER_CA

#if TLS = false don't need to add "--tls --cafile $ORDERER_CA"
peer channel join -o localhost:7050 -b mychannel.block --tls --cafile $ORDERER_CA

#if TLS = false don't need to add "--tls --cafile $ORDERER_CA"
peer channel update -o orderer.noureach.com:7050 -c mychannel -f /opt/gopath/fabric-samples/reach-network/channel-artifacts/org1-anchor.tx --tls --cafile $ORDERER_CA

#go to chaincode folder
# cd /opt/gopath/src/chain

mkdir -p $GOPATH/src/sacc && cd $GOPATH/src/sacc
touch sacc.go
vi sacc.go 
#add chaincode to that file

#create mod.go file 
go mod init sacc.go

#install nesessary depandency
go mod tidy
# go mod vendor
GO111MODULE=on go mod vendor

#list file 
ls -alrt

#change permission file
# chmod 777 mychaincode.go 
# chmod 777 go.mod
# chmod 777 go.sum 

#install chaincode
# CORE_PEER_ADDRESS=peer0.noureach-network.com:7051 peer chaincode install -n sacc -p chain/my-chaincode -v 1.0
CORE_PEER_ADDRESS=peer0.noureach-network.com:7051 peer chaincode install -n sacc -p sacc.go -v 1.0

#list chaincode 
peer chaincode list --installed

#instanttiate chaincode 
# cd /opt/gopath/src/github.com/hyperledger/fabric/peer
peer chaincode instantiate -o orderer.noureach.com:7050 -C mychannel -n sacc -v 1.0 -l golang   -c  '{"Args":["a", "100"]}' -P "OR('Org1MSP.member')"

peer chaincode instantiate -o orderer.noureach.com:7050 -n sacc -v 1.0 -c '{"Args":["reach","100"]}' -C mychannel -P "AND ('Org1MSP.member')"
