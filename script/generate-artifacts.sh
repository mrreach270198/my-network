
export FABRIC_CFG_PATH=$PWD
# crypto-config/ordererOrganizations/noureach.com/msp/

echo "========== Creating genesis block ... =========="
configtxgen -profile SupplyOrdererGenesis -channelID genesis-id -outputBlock ./artifacts/genesis.block 
echo "========== Creating genesis block done!!! =========="

echo "========== Creating channel.tx ... =========="
configtxgen -profile SupplyChannel -channelID mychannel -outputCreateChannelTx ./artifacts/channel.tx
echo "========== Creating channel.tx done!!! =========="

echo "========== Creating anchor peer ... =========="
configtxgen -profile SupplyChannel -channelID mychannel -outputAnchorPeersUpdate ./artifacts/org1-anchor.tx -asOrg Org1MSP
echo "========== Creating anchor peer done!!! =========="

# get data in json
configtxgen --inspectBlock ./artifacts/channel.tx > ./blocks-inspected/channel.tx.json
configtxgen --inspectChannelCreateTx ./artifacts/genesis.block > ./blocks-inspected/genesis.block.json
configtxgen --inspectBlock ./artifacts/org1-anchor.tx > ./blocks-inspected/org1-anchor.tx.json
echo "========== Created inspected folder =========="
