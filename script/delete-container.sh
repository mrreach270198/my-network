sudo docker container stop orderer.noureach.com
sudo docker container stop peer0.noureach-network.com
sudo docker container stop cli

sudo docker container rm orderer.noureach.com
sudo docker container rm peer0.noureach-network.com
sudo docker container rm cli

sudo docker ps -a

sudo docker volume prune
# sudo docker volume rm net_orderer.noureach.com
# sudo docker volume rm net_peer0.noureach-network.com

sudo docker volume ls