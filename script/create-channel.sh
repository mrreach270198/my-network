#export binary
export PATH=${PWD}/bin:$PATH

#export CORE_PEER_MSPCONFIGPATH
export CORE_PEER_MSPCONFIGPATH=${PWD}/crypto-config/peerOrganizations/noureach.com/users/Admin@noureach.com/msp

#create channel
peer channel create -o localhost:7050 -c mychannel -f ./artifacts/channel.tx
