#up docker 
sudo docker-compose -f docker-compose-cli.yaml up -d

#interact with cli tool
sudo docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" -e "CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/fabric-samples/reach-network/crypto-config/peerOrganizations/noureach.com/peers/devpeer/tls/ca.crt" -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/fabric-samples/reach-network/crypto-config/peerOrganizations/noureach.com/users/Admin@noureach.com/msp" -e "CORE_PEER_ADDRESS=peer0.noureach-network.com:7051" -it cli bash

export CORE_PEER_MSPCONFIGPATH=/opt/gopath/fabric-samples/reach-network/crypto-config/peerOrganizations/noureach.com/users/Admin@noureach.com/msp

export ORDERER_CA=/opt/gopath/fabric-samples/reach-network/crypto-config/ordererOrganizations/noureach.com/orderers/orderer.noureach.com/msp/tlscacerts/tlsca.noureach.com-cert.pem

#if TLS = false don't need to add "--tls --cafile $ORDERER_CA"
# peer channel create -o orderer.noureach.com:7050 -c mychannel -f /opt/gopath/fabric-samples/reach-network/channel-artifacts/channel.tx --tls --cafile $ORDERER_CA
peer channel create -o orderer.noureach.com:7050 -c mychannel -f /opt/gopath/fabric-samples/reach-network/channel-artifacts/channel.tx

#if TLS = false don't need to add "--tls --cafile $ORDERER_CA"
# peer channel join -b mychannel.block --tls --cafile $ORDERER_CA
peer channel join -b mychannel.block

#if TLS = false don't need to add "--tls --cafile $ORDERER_CA"
# peer channel update -o orderer.noureach.com:7050 -c mychannel -f /opt/gopath/fabric-samples/reach-network/channel-artifacts/org1-anchor.tx --tls --cafile $ORDERER_CA
peer channel update -o orderer.noureach.com:7050 -c mychannel -f /opt/gopath/fabric-samples/reach-network/channel-artifacts/org1-anchor.tx

mkdir -p $GOPATH/src/sacc && cd $GOPATH/src/sacc

touch mychaincode.go

# vi sacc.go 

cp /opt/gopath/src/chain/my-chaincode/mychaincode.go /opt/gopath/src/sacc/mychaincode.go

#create mod.go file 
go mod init mychaincode.go

#install nesessary depandency
go mod tidy
# go mod vendor
GO111MODULE=on go mod vendor

peer lifecycle chaincode package mycc.tar.gz --path ./ --lang golang --label mycc_1

peer lifecycle chaincode install mycc.tar.gz

peer lifecycle chaincode queryinstalled

export CC_PACKAGE_ID=mycc_1:e3fd3a459d95d5230359c52a33b0ad6b914ad1cb009225b25af6497a790bf31c

peer lifecycle chaincode approveformyorg --channelID mychannel --name mycc --version 1.0 --init-required --package-id $CC_PACKAGE_ID --sequence 1 

peer lifecycle chaincode checkcommitreadiness --channelID mychannel --name mycc --version 1.0 --sequence 1 --output json --init-required

peer lifecycle chaincode commit -o orderer.noureach.com:7050 --channelID mychannel --name mycc --version 1.0 --sequence 1 --init-required --peerAddresses peer0.noureach-network.com:7051

peer lifecycle chaincode querycommitted --channelID mychannel --name mycc

peer chaincode invoke -o orderer.noureach.com:7050 --isInit -C mychannel -n mycc --peerAddresses peer0.noureach-network.com:7051 -c '{"Args":["InitLedger",""]}' --waitForEvent

peer chaincode query \
-C mychannel -n mycc \
-c '{"Args":["GetAllAssets"]}'

peer chaincode query -C mychannel -n mycc -c '{"Args":["ReadAsset","asset1"]}'

peer chaincode invoke \
-o orderer.noureach.com:7050 -C mychannel -n mycc \
--peerAddresses peer0.noureach-network.com:7051 \
-c '{"function": "CreateAsset", 
"Args":["asset77", "GREEN","7", "REACH", "77"]}'

peer chaincode query -C mychannel -n mycc -c '{"Args":["GetAllAssets"]}'


# ls
#install chaincode
# CORE_PEER_ADDRESS=peer0.noureach-network.com:7051 peer chaincode install -n sacc -p chain/my-chaincode -v 1.0
# CORE_PEER_ADDRESS=peer0.noureach-network.com:7051 peer chaincode install -n sacc -p sacc.go -v 1.0

#list chaincode 
# peer chaincode list --installed

# peer chaincode instantiate -n sacc -v 1.0 -c '{"Args":["reach","100"]}' -C mychannel -P "AND ('Org1MSP.member')"
